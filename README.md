## GET TO WORK ON ANY NUMBERCRUNCHER (that runs a minority o.s.)

### What is this repository for? ###

Bootstrap a reasonably up-to-date ubuntu environment for software development, according to my latest cargo cult adoptions

### How do I get set up? ###

```
git clone git clone https://r0pewalker@bitbucket.org/r0pewalker/portable-conf.git
cd portable-conf
./install.sh
```

### What do I find in here? ###

-editor: vim  
-term: urxvt  
-multiplexer: tmux  
-vim plugins for python development  
-TBD: virtualenv setup  
-TBD: docker development setup  
-TBD: vm setup  
-TBD: vim plugins for python3 development  
-TBD: vim plugins for go development  
-TBD: some kind of test framework  

### Who do I talk to? ###

Trees. Trees are the best of both worlds.  
Second best is <biassoni.r@protonmail.com>
