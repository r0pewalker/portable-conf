# up to date for ubuntu 16.04.1
sudo apt-get update && apt-get install -y \
apt-utils \
gcc \
make \
build essential \
git \
linux-image-extra-$(uname -r) \
linux-image-extra-virtual \
docker-engine \
docker-compose \
virtualbox \
virtualbox-guest-dkms \
virtualbox-guest-additions-iso \
python-software-properties \
software-properties-common \
python-virtualenv \
python3.5 \
python3.5-dev \
golang \
libssl-dev \
libffi-dev \
vim \
rxvt-unicode-256color \
tmux \
awscli \
&& apt-get clean
# vim plugins
# pathogen plugin manager
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
# color scheme
git clone https://github.com/morhetz/gruvbox.git ~/.vim/bundle/gruvbox
# syntax checker
git clone https://github.com/vim-syntastic/syntastic.git ~/.vim/bundle/syntastic
# display tags in a separate window
git clone git://github.com/majutsushi/tagbar ~/.vim/bundle/tagbar
# vim status line
# Remember to run :Helptags to generate help tags
git clone https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline
# git wrapper
git clone git://github.com/tpope/vim-fugitive.git ~/.vim/bundle/vim-fugitive
# golang support
git clone https://github.com/fatih/vim-go.git ~/.vim/bundle/vim-go
# yaml indentation
git clone https://github.com/avakhov/vim-yaml ~/.vim/bundle//vim-yaml
# json helper
git clone https://github.com/elzr/vim-json ~/.vim/bundle/vim-json
# docker machine
curl -L https://github.com/docker/machine/releases/download/v0.9.0/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine && chmod +x /tmp/docker-machine && sudo cp /tmp/docker-machine /usr/local/bin/docker-machine
docker-machine create --driver virtualbox default
# docker version manager
curl -sL https://download.getcarina.com/dvm/latest/install.sh | sh
mkdir -p ~/Projects/{Go, Python}
mkdir -p $GOPATH{src,pkg, bin}
# copy and source versioned dotfiles
SOURCELIST=$(ls -a | grep '^\.')
for f in $SOURCELIST
  do 
      cp -r ./$f ~/$f;
      source ~/$f;
  done;
