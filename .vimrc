set tabstop=4     " an hard TAB displays as 4 columns
set expandtab     " insert spaces when hitting TABs
set softtabstop=4 " insert/delete 4 spaces when hitting a TAB/BACKSPACE
set shiftround    " round indent to multiple of 'shiftwidth'
set shiftwidth=4  " operation >> indents 4 columns; << unindents 4 columns
set autoindent    " align the new line indent with the previous line

" Pathogen load
filetype off
call pathogen#infect()
call pathogen#helptags()

" activates filetype detection
filetype plugin indent on

" activates syntax highlighting among other things
syntax on

" Syntastic
" jshint
let g:syntastic_javascript_jshint_exec='/usr/local/bin/jshint'
"pylint
let g:syntastic_pylint_exec='/usr/bin/pylint'
let g:syntastic_always_populate_loc_list=1
" fugitive stuff
" colorscheme 
set t_Co=256
let g:gruvbox_bold=1
let g:gruvbox_italic=0
colorscheme gruvbox
set background=dark 
" Enable folding
set foldmethod=syntax
set foldlevelstart=1
let javaScript_fold=1         " JavaScript
let perl_fold=1               " Perl
let php_folding=1             " PHP
let r_syntax_folding=1        " R
let ruby_fold=1               " Ruby
let sh_fold_enabled=1         " sh
let vimsyn_folding='af'       " Vim script
let xml_syntax_folding=1      " XML
" Enable folding with the spacebar
nnoremap <space> za
" numbers
set nu
" Tagbar
let g:tagbar_autofocus=1
let g:tagbar_autoclose=1
nmap <F8> :TagbarToggle<CR>
" airline
set laststatus=2
" spellcheck for commit messages
autocmd BufRead COMMIT_EDITMSG setlocal spell spelllang=en_us
nnoremap gp `[v`]
